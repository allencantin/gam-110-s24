﻿using System;
using System.Collections.Generic;

public class Soup //blame kyle for this
{
    public string name;
    public bool isItHot;
    public bool isItMeaty;
    public bool doesItPleaseTheShareholders;
    public int cups;
    public Soup()
    {
        name = "boring soup";
        isItHot = false;
        isItMeaty = false;
        doesItPleaseTheShareholders = false;
        cups = 2;
    }
}

class ListPractice
{
    static void Main(string[] args)
    {
        List<Soup> soupList = new List<Soup>();

        Soup sourp = new Soup(); //soup object!
        sourp.name = "Sourp";
        sourp.isItHot = true;
        sourp.isItMeaty = true;
        sourp.cups = 4;
        soupList.Add(sourp);
        soupList.Add(new Soup()); // create a soup object inside the list,  can only be referenced inside the list

        Console.WriteLine("Number of soups: " + soupList.Count);
        Console.WriteLine(soupList[0].name + " : cups " + soupList[0].cups);
        Console.WriteLine(soupList[1].name + " : cups " + soupList[1].cups);
        
        soupList.RemoveAt(1); //remove the second list element

        if (soupList.Contains(sourp))
        {
            Console.WriteLine("WE HAVE SOURP THE SHAREHOLDERS ARE PLEASED");
        }
        else
        {
            Console.WriteLine("boooooooooo");
        }

        List<string> words = new List<string>();
        words.Add(sourp.name);
        words.Add("shareholders");

        string[] moreWords = { "hello", "funny", "what about stew" };

        words.AddRange(moreWords);

        for(int i = 0; i < words.Count; i++)
        {
            Console.WriteLine(words[i]);
        }
    }
}