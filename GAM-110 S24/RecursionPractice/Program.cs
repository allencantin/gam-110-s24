﻿using System;
using RecursionPractice;

class NotABeardClass
{
    //recursion!!
    static float Factorial(float n)
    {
        if (n - 1 > 0)
            return n * Factorial(n - 1);
        else
            return n;
    }

    //recursion!!
    static int Fibonacci(int n)
    {
        if (n < 2)
            return n;
        else
            return Fibonacci(n - 1) + Fibonacci(n-2);
    }

    //example
    static void PrintSillyThings(int n)
    {
        Console.WriteLine("tee hee!");
        if (n - 1 > 0)
            PrintSillyThings(n - 1);
    }

    static void Main(string[] args)
    {
        PrintSillyThings(10);
        Console.WriteLine(Factorial(10));
        Console.WriteLine(Fibonacci(10));
        CoolMath maths = new CoolMath();
    }
}