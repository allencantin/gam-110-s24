﻿using System;

class FunctionPractice
{
    //a function that adds numbers together
    static int Addition(int num1, int num2, int num3) //3 ints as arguments
    {
        return (num1 + num2 + num3); //output the sum of the arguments
    }

    //they gonna have soup at the function?
    static bool Soup(string input, bool isThereSoup, float soupNumber)
    {
        //this function determines based on what soup is the input if there is soup, and if so how much
        if (input == "Tomato Soup")
        {
            if (isThereSoup == false && soupNumber > 0)
            {
                return true;
            }
            else if (soupNumber * 3 > 8)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (input == "Chicken Noodle Soup")
        {
            return false; //Kyle ate it all :(
        }
        else if (input == "Hamburger Soup")
        {
            return true; //we have a lot and nobody wants it
        }
        else
        {
            return true; //we cannot comprehend this soup. it's out of this world!!
        }
    }


    static void Main(string[] args)
    {
        Console.WriteLine("ADDITION!!!");
        //call the addition function
        Console.WriteLine( Addition(3, 15, 111) );
        Console.WriteLine("Will there be soup at the function?");
        Console.WriteLine(Soup(Console.ReadLine(), false, 22.3f));
    }
}