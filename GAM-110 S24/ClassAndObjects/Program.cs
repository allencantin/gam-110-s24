﻿using System;

public /*abstract*/ class Animal
{
    public string Name;
    public bool alive;
    public bool mammal;
    protected bool warmBlooded;
    public bool hasFur;
    //enums!!
    public enum DietType
    {
        Herbivore,
        Omnivore,
        Carnivore
    }
    public DietType diet;
    public int numLegs;
    public bool doesPollution;


    //default constructor
    public Animal() 
    {
        Name = "Wilbert George";
        alive = true;
        mammal = false;
        warmBlooded = false;
        hasFur = false;
        diet = DietType.Omnivore;
        //numLegs = 51;
        //random legs
        Random randy = new Random();
        numLegs = randy.Next(200);
        doesPollution = false;
    }

    //overload constructor
    public Animal(string newName, bool newAlive, bool newMammal, bool newWarmBlooded, bool newHasFur, DietType newDiet, int newNumLegs, bool newDoesPollution )
    {
        Name = newName;
        alive = newAlive;
        mammal = newMammal;
        warmBlooded = newWarmBlooded;
        hasFur = newHasFur;
        diet = newDiet;
        numLegs = newNumLegs;
        doesPollution = newDoesPollution;
    }


}

// frog inherits properties from the animal class
public class Frog : Animal
{
    public Frog()
    {
        Name = "Mr. Frog";
        numLegs = 4;
        warmBlooded = false;
    }

    public void Ribbit()
    {
        Console.WriteLine("RIBBIT");
    }
}



class Program
{
    static void Main(string[] args)
    {
        //make an animal object!
        
        //default
        Animal wilbert = new Animal();

        //overload
        Animal sheep = new Animal("Sheepy", true, true, true, true, Animal.DietType.Herbivore, 4, false);

        //inherited class object
        Frog frogman = new Frog();

        Console.WriteLine( wilbert );
        Console.WriteLine(wilbert.Name);
        Console.WriteLine(wilbert.numLegs);


        Console.WriteLine( sheep );
        Console.WriteLine(sheep.Name);
        Console.WriteLine(sheep.numLegs);

        Console.WriteLine( frogman );
        Console.WriteLine(frogman.Name);
        Console.WriteLine(frogman.numLegs);
    }
}