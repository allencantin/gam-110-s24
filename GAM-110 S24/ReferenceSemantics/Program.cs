﻿using System;

class Car
{
    public string name;
    public int HP;
    public Car(string name, int hP)
    {
        this.name = name;
        HP = hP;
    }
}


class Program
{
    static void CarCrash(ref Car car1, ref Car car2, out string crashResult) //ref keywords pass a reference to the object!
    {
        //the car with less HP takes more damage?
        if (car1.HP < car2.HP)
        {
            //do more damage to car 1
            car1.HP -= 1000;
            car2.HP -= 500;
            crashResult = car2.name + " won the crash!";
        }
        else
        {
            //do more damage to car 2
            car1.HP -= 500;
            car2.HP -= 1000;
            crashResult = car1.name + " won the crash!";
        }
    }

    static void Main(string[] args)
    {
        Car allensCar = new Car("2000 Toyota Corolla", 2000);
        Car lainsCar = new Car("2004 Toyota Camry", 4000);
        string result = "";
        //before car crash
        Console.WriteLine("Allen Car: " + allensCar.HP);
        Console.WriteLine("Lain Car: " + lainsCar.HP);

        Console.WriteLine("oh no!");
        CarCrash(ref allensCar, ref lainsCar, out result); //use ref in function call!
                                                //use out in function call
        //after car crash
        Console.WriteLine("Allen Car: " + allensCar.HP);
        Console.WriteLine("Lain Car: " + lainsCar.HP);
        Console.WriteLine(result);
    }
}