﻿using System;

class Program
{
    static void Main(string[] args) //ENTRYPOINT
    {
        /*
         * multiple
         * lines
         * comment
         */

        Console.WriteLine("Hello World!");
        Console.WriteLine("Does Fallout 76 still have bugs?");

        //this is a string variable!
        string input = Console.ReadLine();

        Console.WriteLine("This was the user's input: " + input);

        //Console.Beep();


        //how we take in integers through the console
        Console.WriteLine("What year were you born in?");
        //input = Console.ReadLine();
        //use try parse for a safer way!
        //int birthYear = 0;
        //int.TryParse(input, out birthYear);

        int birthYear = int.Parse(Console.ReadLine());  //this will work as long as the input is a number
        


        //cool math functions!!!
        Console.WriteLine( MathF.Min(10, birthYear));

    }
}