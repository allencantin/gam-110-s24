﻿using System;
using System.Collections.Generic;
using System.IO;

class DocumentReading
{
    static void Main(string[] args)
    {
        //where all the words go
        Dictionary<string, int> wordCount = new Dictionary<string, int>();
        StreamReader reader = new StreamReader("Baljuna Covenant.txt"); //opening a file to read
        
        if (File.Exists("Baljuna Covenant.txt")) //for safety
        {
            while (reader.Peek() != -1) //looking until there's nothing left
            {
                string currentWord = "";
                int output = reader.Read();
                char letter = (char)output;
                //use Read() to construct words char by char

                //use an if statement to separate space char for words
                if (reader.Read() == ' ')
                {
                    //we need to check if the current word exists in the dictionary
                    if (wordCount.ContainsKey(currentWord))
                    {
                        //the current word's count in the dictionary is increased
                        wordCount[currentWord] = wordCount[currentWord] + 1;
                    }
                    //if the word doesn't exist in the dictionary yet
                    else
                    {
                        wordCount.Add(currentWord, 1);
                    }
                }
                //character is not a space
                else
                {

                }
            }
        }
        
    }
}