﻿using System;

class Debugging
{
    static void Main(string[] args)
    {
        int[] leaderboard = { 125000, 90000, 80000, 100, 0 };

        for (int i = 0; i < leaderboard.Length; i++)
        {
            Console.WriteLine(leaderboard[i]);
        }

        int NEW_HISCORE = 100000;

        for (int i = 0; i < leaderboard.Length; i++)
        {
            if (NEW_HISCORE > leaderboard[i])
            {
                int p_score = leaderboard[i];
                leaderboard[i] = NEW_HISCORE;
                int temp = p_score;
                for (int f = i+1; f < leaderboard.Length; f++)
                {
                    p_score = temp;
                    if (p_score > leaderboard[f])
                    {
                        temp = leaderboard[f];
                        leaderboard[f] = p_score;
                    }
                }
                break;
            }
        }

        for (int i = 0; i < leaderboard.Length; i++)
        {
            Console.WriteLine(leaderboard[i]);
        }

    }
}