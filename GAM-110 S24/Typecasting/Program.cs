﻿using System;

class Typecasting
{
    static void Main(string[] args)
    {
        //change our output encoding
        Console.OutputEncoding = System.Text.Encoding.Unicode;

        int someCharNumber = 'A'; //implicit typecast
        Console.WriteLine(someCharNumber);

        char someIntChar = (char)219; //explicit typecast
        Console.WriteLine(someIntChar);

        Console.ReadLine();

        for(int i = 30; i < 1000; i++)
        {
            Console.WriteLine(i + ": " + (char)i);
        }

        //39 -> thirty-nine


    }
}