﻿using System;

abstract class Food
{
    public string Name;
    //public string taste;

    //taste enum
    public enum TasteType
    {
        sweet,
        sour,
        salty,
        bitter,
        umami
    };
    public TasteType taste;

    public int calories;
    public float volume;

    public virtual void Eat(float bite) //function to be overridden
    {
        Console.WriteLine("MUNCH");
    }
}

class Burger : Food
{
    public Burger(string newName, TasteType newTaste, int newCal, float newVol)
    {
        Name = newName;
        taste = newTaste;
        calories = newCal;
        volume = newVol;
    }

    public override void Eat(float bite) //here we are overriding the base definition of this function
    {
        base.Eat(bite);
        Console.WriteLine("mmmm burger");

        volume -= bite;
    }
}

class Cereal : Food
{
    public Cereal(string newName, TasteType newTaste, int newCal, float newVol)
    {
        Name = newName;
        taste = newTaste;
        calories = newCal;
        volume = newVol;
    }

    public override void Eat(float bite)
    {
        base.Eat(bite);
        Console.WriteLine("good morning cereal :D");

        volume -= bite + 2;
    }
}

class Soda : Food
{
    public Soda(string newName, TasteType newTaste, int newCal, float newVol)
    {
        Name = newName;
        taste = newTaste;
        calories = newCal;
        volume = newVol;
    }

    public override void Eat(float bite)
    {
        volume -= bite;
        if (taste == TasteType.sour)
            Console.WriteLine("jeez that's sour");
        else
            Console.WriteLine("slurp! big sip");
    }
}

class CoolnessOnTheEclipseDay
{
    static void Main(string[] args)
    {
        Burger burger = new Burger("slammin' burger", Food.TasteType.umami, 600, 180);
        Cereal cereal = new Cereal("frosted flakes", Food.TasteType.sweet, 200, 200);
        Soda soda = new Soda("mr. pibb", Food.TasteType.sweet, 250, 50);

        List<Food> table = new List<Food> { burger, cereal, soda };

        for (int i = 0; i < table.Count; i++)
        {
            table[i].Eat(10);
        }
    }
}