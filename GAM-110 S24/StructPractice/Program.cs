﻿using System;

class Program
{
    struct US_Currency
    {
        //make a struct to represent the US Currency
        //dollars and cents
        public int dollars, cents;

        //Constructor(dollars, cents)
        public US_Currency(int newDollars, int newCents)
        {
            dollars = newDollars;
            cents = newCents;
        }

        //Add(dollars, cents)
        public void Add(int addDollars, int addCents)
        {
            dollars += addDollars;
            cents += addCents;
            while (cents >= 100) //carrying over 100 cents to a dollar
            {
                cents -= 100;
                dollars++;
            }
        }
    }

    static void Main(string[] args)
    {
        US_Currency wallet = new US_Currency(7, 20);
        wallet.Add(3, 87);
        wallet.Add(0, 493);
        Console.WriteLine("$" + wallet.dollars + "\t" + wallet.cents + "cents");
    }
}