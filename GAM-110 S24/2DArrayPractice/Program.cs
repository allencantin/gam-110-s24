﻿using System;

class Program
{
    static void Main(string[] args)
    {
        //2D ARRAYS

        int[,] map =
        {
            { 1, 1, 1, 1, 1},
            { 1, 0, 0, 0, 1},
            { 1, 2, 0, 0, 1},
            { 1, 0, 0, 0, 1},
            { 1, 1, 1, 1, 1}
        };

        //print this whole array!
        //use nested for loops!
        //maps GetLength() gives us the length of one array or the internal arrays
        for(int row=0; row<map.GetLength(0); row++)
        {
            //another loop to go through the inside array elements
            for(int col=0; col<map.GetLength(1); col++)
            {
                Console.Write(map[row, col]);
            }

            Console.WriteLine();
        }
    }
}