﻿using System;

class Program
{
    static void Main(string[] args)
    {
        //ARRAYS!!!
        int[] scoreboard = new int[3]; //this makes an array of 3 integers
        string[] names = {"Billy", "tim", "Jane", "Scrumbo", "spleens" };

        for(int i = 0; i < scoreboard.Length; i++)
        {
            scoreboard[i] = 0;
        }

        int awesomePlayerScore = 500000;

        //foreach (int score in scoreboard) //this also iterates through the array!
        //{
        //    if (score < awesomePlayerScore)
        //    {
        //        score = awesomePlayerScore;
        //    }
        //}

        for(int i = 0; i < scoreboard.Length; i++)
        {
            if (scoreboard[i] < awesomePlayerScore)
            {
                scoreboard[i] = awesomePlayerScore;
                break; //stops the current loop
                //it wants to break free
            }
        }

        foreach (int score in scoreboard)
        {
            Console.WriteLine(score);
        }

    }
}
