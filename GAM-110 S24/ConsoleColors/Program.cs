﻿using System;

class Coloring
{
    static void Main(string[] args)
    {
        //sets background of typed characters
        Console.BackgroundColor = ConsoleColor.Yellow;

        Console.WriteLine("normal human text");

        //green text
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("alien text");

        //pink text
        Console.ForegroundColor = ConsoleColor.Magenta;
        Console.WriteLine("this is barbie text");

        //reset console colors
        Console.ResetColor();
        Console.WriteLine("normal again");
    }
}