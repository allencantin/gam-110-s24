﻿using System;

class Program
{
    static void Main(string[] args)
    {
        //while loop
        bool currentlyPlaying = true;
        while (currentlyPlaying)
        {
            Console.WriteLine("hello hero! we are dancing killing slimes!");
            Console.WriteLine("rearranging furniture maybe");

            Console.WriteLine("What do you want to do?");
            Console.WriteLine("1) Dancing");
            Console.WriteLine("2) Killing slimes");
            Console.WriteLine("3) Rearranging Furniture");
            Console.WriteLine("4) QUIT");

            string choice = Console.ReadLine();
            int numberChoice;
            //cool way to use try parse
            while(!int.TryParse(choice, out numberChoice))
            {
                Console.WriteLine("that's not a choice, dude");
                Console.WriteLine("What do you want to do?");
                Console.WriteLine("1) Dancing");
                Console.WriteLine("2) Killing slimes");
                Console.WriteLine("3) Rearranging Furniture");
                Console.WriteLine("4) QUIT");
                choice = Console.ReadLine();
            }


            switch(numberChoice)
            {
                case 1:
                    for(int i = 0; i < 3; i++) //for loop!
                    {
                        Console.WriteLine("DANCING");
                    }
                    break;
                case 2:
                    Console.WriteLine("You kill a bunch of slimes. You are now " +
                        "covered in slime");
                    break;
                case 3:
                    Console.WriteLine("this joke is !funny");
                    break;
                case 4:
                    currentlyPlaying = false;
                    break;
            }



        }
    }
}