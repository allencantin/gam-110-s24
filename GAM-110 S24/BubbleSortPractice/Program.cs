﻿using System;
using System.Diagnostics; //allows us to use stopwatch

class BubbleSortPractice
{

    static void Main(string[] args)
    {
        int[] theArray = new int[20];
        Random rand = new Random();

        //random initialization
        for (int i = 0; i < 20; i++)
        {
            theArray[i] = rand.Next(31);
        }

        Stopwatch sw = new Stopwatch(); //cool new stopwatch object for timing processes
        
        //print array
        Console.Write("Our unsorted array is ");

        for(int i = 0; i < theArray.Length; i++)
        {
            Console.Write(theArray[i] + " ");
        }


        int temp; //for storing temporary values that are switching
        sw.Start();//this starts the stopwatch timer RIGHT HERE

        //now we can sort
        for (int i = 0; i < theArray.Length; i++) //how many passthroughs
        {
            temp = theArray[i];
            //this one does the sorting
            for (int s = 0; s < theArray.Length - 1; s++)
            {
                if (theArray[s] > theArray[s + 1])
                {
                    //big swapping step
                    temp = theArray[s + 1];
                    theArray[s + 1] = theArray[s];
                    theArray[s] = temp;
                }
            }
        }
        sw.Stop();//stops the stopwatch timer RIGHT HERE
        Console.WriteLine("Sorted array ");
        for (int i = 0; i < theArray.Length; i++)
        {
            Console.Write(theArray[i] + " ");
        }
        Console.WriteLine();
        Console.WriteLine("Time taken " + sw.Elapsed); //for printing stuff

    }

}