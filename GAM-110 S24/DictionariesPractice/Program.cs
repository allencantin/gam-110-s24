﻿using System;
using System.Collections.Generic; //this gives us lists and dictionaries!
using System.IO;

class HiScorePractice
{
    static void Main(string[] args)
    {
        //dictionary for our high scores
        Dictionary<string, int> hiscores = new Dictionary<string, int>(); //initialized the dictionary

        //initialize with values
        //Dictionary<string, int> hiscores = new Dictionary<string, int>() {
        //    {"OMG", 108000 }, 
        //    {"TOM", 98000 }
        //};

        hiscores.Add("OMG", 108000);
        hiscores.Add("TOM", 98000);
        hiscores.Add("JER", 75000);

        //count is the same as lists - how many entries are in there
        Console.WriteLine(hiscores.Count);

        Console.WriteLine("HISCORES");
        //foreach loops are really helpful for dictionaries!
        foreach (KeyValuePair<string, int> pair in hiscores)
        {
            //print out entries
            if (pair.Value > 100000)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
            }
            else if (pair.Value > 90000)
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
            }
            Console.WriteLine(pair.Key + "\t" + pair.Value);
        }
        Console.ResetColor();

        //write scores to external file
        StreamWriter writer = new StreamWriter("scores.txt"); //initialized my writer to a filepath
        
        //write data to file!
        foreach (KeyValuePair<string, int> pair in hiscores)
        {
            writer.WriteLine(pair.Key + "\t" + pair.Value);
        }

        writer.Flush(); //makes sure the data is saved to the file
        writer.Close(); //closes the stream, frees memory used by 

        Console.ReadLine();
    }
}